import re
import random
import scipy
from collections import deque
import numpy as np
import pandas as pd
import networkx as nx  # 导入建网络模型包，命名nx
import matplotlib.pyplot as plt  # 导入科学绘图宝

"""
content="12What are you doing? 23te12?"
regu_cont=re.compile(r"^\d+")
print(regu_cont)
yl=regu_cont.match(content)
print(yl)
if yl:
    print (yl.group(0))
else:
    print ("what happen?")
"""
G = nx.Graph()  # 创建一人空的复杂网络，包含nodes，edges
# G.add_nodes_from([1,2,3,4,0])
# G.add_edge(1,3)
# fp=open(".\\higgs-reply_network\\higgs-reply_network.txt","r")
fp = open("./higgs-reply_network/higgs-social_network.txt", "r")

# fp=open("D:\\fct.txt")
# for line in fp.readlines():

while True:
    content = fp.readline()
    if len(content) == 0:  # 若文件提取的为空字符串，则跳出读取文件的循环
        break
    else:
        str_list = content.split()  # 分割字符串
        node = int(str_list[0])
        edge_node = int(str_list[1])
        G.add_node(node)  # 将节点信息存入复杂网络中
        G.add_node(edge_node)
        G.add_edge(node, edge_node)  # 将连边信息存入复杂网络中
# print('Nodes',G.nodes)   #顺序并非是按照从小到大的
# print(len(G.nodes))
# print(len(G.edges))
# Node_num=len(G.nodes)
# print('Edges',G.edges)

'''
for n in G.nodes:
    nei_list=list(G.neighbors(n))
    print(nei_list)
'''


# nx.draw_networkx(G)
# nx.draw_networkx_edges(ws, pos=pos, width=0.3, alpha=0.6)
# nx.draw_networkx(ws, pos=pos, with_labels=True, alpha=0.6)
# plt.show()

def search(start_node):
    nodes_num = 1 #记录加入的节点数 不过也可以删掉 因为后面是直接用len(G_new.nodes)获取新图的节点数
    search_queue = deque() #初始化一个队列存储当前的点的邻居节点集合
    search_queue += list(G.neighbors(start_node)) #将当前点的邻居节点集合加入队列中
    G_new = nx.Graph() #初始化一个新图
    '''
    for i in list(G.neighbors(start_node)):
        G_new.add_node(start_node)    #将节点信息存入复杂网络中
        G_new.add_node(i)
        G_new.add_edge(start_node,i)  #将连边信息存入复杂网络中
        nodes_num += 1
    '''
    while search_queue:
        current_node = search_queue.popleft() #将第一个邻居节点出队列
        # 如果节点数已经到1000 则直接退出 返回新图
        if len(G_new.nodes) > 1000:
            print('1 ', len(G_new.nodes))
            return G_new
        else:
            #将当前节点的邻居节点加入队列
            search_queue += list(G.neighbors(current_node))
            for i in list(G.neighbors(current_node)):
                random_num = random.random()
                #random_num是0-1的随机数  下面的0.5是设计的随机的界限 可以调整
                if random_num > 0.5:
                    G_new.add_node(current_node)  # 将节点信息存入复杂网络中
                    G_new.add_node(i)
                    G_new.add_edge(current_node, i)  # 将连边信息存入复杂网络中
                    nodes_num += 1
                    # 如果在途中 节点数到达1000 也直接返回
                    if len(G_new.nodes) == 1000:
                        print('2 ', len(G_new.nodes))
                        return G_new


a = search(677)
print(len(a.nodes))
print(a.nodes)
# nx.draw_networkx(a)
# nx.draw_networkx_edges(ws, pos=pos, width=0.3, alpha=0.6)
nx.draw_networkx(a, with_labels=False, alpha=0.6)
plt.show()

'''
优点：
BFS对因为没有使用递归 对空间占用较小
简单易于理解
不会有递归深度过高 爆栈的问题 
'''